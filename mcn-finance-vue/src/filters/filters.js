export default {
  filters: {
    getMonth(value, arg1) {
      const month = [
        { short: 'jan', long: 'January' },
        { short: 'feb', long: 'February' },
        { short: 'mar', long: 'March' },
        { short: 'apr', long: 'April' },
        { short: 'may', long: 'May' },
        { short: 'jun', long: 'June' },
        { short: 'jul', long: 'July' },
        { short: 'aug', long: 'August' },
        { short: 'sept', long: 'September' },
        { short: 'oct', long: 'October' },
        { short: 'nov', long: 'November' },
        { short: 'dec', long: 'December' },
      ];
      return month[value][arg1];
    },
  },
};
