export default {
  methods: {
    convertObjectToArray(obj) {
      const arr = Object.keys(obj).map(k => obj[k]);
      return arr;
    },
    roundOff(value, power) {
      const multiplier = 10 ** power;
      return (Math.round(value * multiplier)) / multiplier;
    },
  },
};
