// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import VueResource from 'vue-resource';
import vueNumeralFilterInstaller from 'vue-numeral-filter';
import Vue from 'vue';
import App from './App';

Vue.config.productionTip = false;
Vue.use(VueResource);
Vue.use(vueNumeralFilterInstaller, { locale: 'en-gb' });
Vue.http.headers.common['Access-Control-Allow-Origin'] = '*';

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>',
});
